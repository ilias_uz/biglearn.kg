"""courses URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from app.views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', index, name='index'),
    url(r'^home/', home, name='home'),
    url(r'^update_course/(?P<pk>\d+)/', CourseUpdate.as_view(), name='update'),
    url(r'^delete_course/(?P<pk>\d+)/', DeleteCourse.as_view(), name='delete'),
    url(r'^user_panel/', user_panel, name='user_panel'),
    url(r'^book_it/(?P<id>\d+)/', book_it, name='book_it'),
    url(r'^registration/', registration, name='registration'),
    url(r'^registration_course/', registration_course, name='registration_course'),
]
