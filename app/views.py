# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from forms import Registration, RegistrationCourse, RegistrationInCourse
from django.contrib.auth import authenticate, login as fn_login
from django.views.generic.edit import UpdateView
from django.views.generic import DeleteView
from models import Client, Course, CourseType, City
from django.core.mail import send_mail
# Create your views here.


class CourseUpdate(UpdateView):
	model = Course
	fields =  ['course_name', 'user_age_from', 'user_age_to', 'course_price_for_moth', 'city_name', 'addres', 'phone_number', 'course_description', 'course_open']
	template_name = 'update_course.html'
	success_url = '/user_panel'


class DeleteCourse(DeleteView):
	model = Course
	success_url = '/user_panel'

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)



def index(request):
	if request.POST:
		user = authenticate(username=request.POST['username'], password=request.POST['password'])
		if user is not None:
			fn_login(request, user)
			return HttpResponseRedirect('/user_panel')
		else:
			return render(request, 'index.html', {'error': u'Имя пользователя или пароль не правильный'})
	return render(request, 'index.html')


def home(request):
	courses = Course.objects.all()
	course_type = CourseType.objects.all()
	city_list = City.objects.all()
	if request.POST:
		age = int(request.POST.get('age'))
		course = request.POST.get('course')
		city = request.POST.get('city')
		print age, course, city
		courses = Course.objects.filter(course_name__course_name=course, user_age_to__gte=age, user_age_from__lte=age, city_name__city_name=city)
	return render(request, 'home.html', {'courses': courses, 'course_type': course_type, 'city_list': city_list})


def book_it(request, id):
	course = Course.objects.get(pk=id)
	title = u'Забраноирование выбранного курса'
	if request.method == 'POST':
		form = RegistrationInCourse(request.POST)
		if form.is_valid():
			form = form.save(commit=False)
			form.course = course
			user_email = course.user_created.email
			if form.external_info != '':
				external_info = form.external_info
			else:
				external_info = 'не указано!'
			subject = u'Родители забронировали место для своего ребенка %s %s с возрастом %s лет. Номер телефон родителя %s. Доп.инфо %s' % (form.child_name, form.child_last_name, form.child_age, form.parrents_phone_number, external_info)
			send_mail(
				'Забронирование курса',
				subject,
				'kazikhodzhaev_i@iuca.kg',
				[user_email],
				fail_silently=False,
			)
			form.save()
			return HttpResponseRedirect('/home')
	else:
		form = RegistrationInCourse()
	return render(request, 'registration.html', {'form': form, 'title': title})


def user_panel(request):
	client = Client.objects.filter(username=request.user.username)
	print len(client)
	if request.user.is_authenticated() and client:
		courses = Course.objects.filter(user_created__username=request.user.username)
		return render(request, 'user_panel.html', {'courses': courses})
	if request.POST:
		user = authenticate(username=request.POST['username'], password=request.POST['password'])
		if user is not None:
			fn_login(request, user)
			return HttpResponseRedirect('/user_panel')
		else:
			return render(request, 'user_login.html', {'error': u'Имя пользователя или пароль не правильный'})
	return render(request, 'user_login.html')


def registration(request):
	if request.method == 'POST':
		form = Registration(request.POST)
		if form.is_valid():
			user = form.save(commit=False)
			user.set_password(request.POST.get('password'))
			user.save()
			user_login = authenticate(username=request.POST['username'], password=request.POST['password'])
			fn_login(request, user_login)
			form.save()
			return HttpResponseRedirect('/')
	else:
		form = Registration()
	return render(request, 'registration.html', {'form': form, 'title':u'Регистрация пользователя'})


def registration_course(request):
	if request.method == 'POST':
		form = RegistrationCourse(request.POST)
		if form.is_valid():
			form = form.save(commit=False)
			user = Client.objects.get(username=request.user.username)
			form.user_created = user
			form.course_open = True
			form.save()
			return HttpResponseRedirect('/user_panel')
	else:
		form = RegistrationCourse()
	return render(request, 'registration.html', {'form': form, 'title':u'Регистрация курса'})

