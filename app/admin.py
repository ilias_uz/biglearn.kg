# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import *
# Register your models here.

admin.site.register(Client)
admin.site.register(Course)
admin.site.register(CourseType)
admin.site.register(City)
admin.site.register(GetCourse)