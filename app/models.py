# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils.encoding import python_2_unicode_compatible
# Create your models here.


@python_2_unicode_compatible
class CourseType(models.Model):
	course_name = models.CharField(max_length=200)

	def __str__(self):
		return self.course_name


@python_2_unicode_compatible
class City(models.Model):
	city_name = models.CharField(max_length=200)

	def __str__(self):
		return self.city_name


class Client(User):
	user_phone = models.CharField(max_length=200)

	def __str__(self):
		return 'UserName: %s, Email: %s' % (self.username, self.email)


@python_2_unicode_compatible
class Course(models.Model):
	user_created = models.ForeignKey(Client)
	course_name = models.ForeignKey(CourseType, verbose_name='Вид занятия')
	user_age_from = models.IntegerField(verbose_name='Возраст от')
	user_age_to = models.IntegerField(verbose_name='Возраст до')
	course_price_for_moth = models.IntegerField(verbose_name='Цена за месяц')
	phone_number = models.CharField(max_length=200, verbose_name='Номер телефона', blank=True)
	course_open = models.BooleanField(verbose_name='Набор продолжается')
	addres = models.CharField(max_length=200, verbose_name='Адрес')
	city_name = models.ForeignKey(City, verbose_name='Город')
	course_description = models.TextField(verbose_name='Дополнительное информация')

	def __str__(self):
		return 'User %s is created %s' % (self.user_created.username, self.course_name.course_name)


@python_2_unicode_compatible
class GetCourse(models.Model):
	course = models.ForeignKey(Course)
	child_name = models.CharField(max_length=200, verbose_name='Имя ребенка')
	child_last_name = models.CharField(max_length=200, verbose_name='Фамилия ребенка')
	child_age = models.IntegerField(verbose_name='Возраст ребенка')
	parrents_phone_number = models.CharField(max_length=200, verbose_name='Номер телефона родителей')
	external_info = models.TextField(blank=True, verbose_name='Дополнительное информация')

	def __str__(self):
		return '%s registereted in %s course' % (self.child_name, self.course.course_name)