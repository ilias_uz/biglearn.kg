# -*- coding: utf-8 -*-
from django import forms
from models import *


class Registration(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput())
	class Meta:
		model = Client
		fields = ['first_name', 'last_name', 'email', 'user_phone', 'username', 'password']


class RegistrationCourse(forms.ModelForm):
	class Meta:
		model = Course
		fields = ['course_name', 'user_age_from', 'user_age_to', 'course_price_for_moth', 'city_name', 'addres', 'phone_number', 'course_description']
		exclude = ('user_created', 'course_open')


class RegistrationInCourse(forms.ModelForm):
	class Meta:
		model = GetCourse
		fields = ['child_name', 'child_last_name', 'child_age', 'parrents_phone_number', 'external_info']
		exclude = ('course', )